package main.java.com.guessNumber.utils;

import java.util.Random;

public class RandomNumGenerator {
    private Random random;
    private int minNum;
    private int maxNum;
    private int range;
    private int randomNumber;

    public RandomNumGenerator(Random random){
        this.random = random;
    }

    public int getRandomNumInRange(int minNum, int maxNum){
        this.minNum = minNum;
        this.maxNum = maxNum;
        this.range = maxNum - minNum + 1;
        randomNumber = random.nextInt(range) + minNum;
        return randomNumber;
    }
}
