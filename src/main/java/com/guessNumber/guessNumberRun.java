package main.java.com.guessNumber;

import main.java.com.guessNumber.services.GuessNumberGame;
import main.java.com.guessNumber.utils.RandomNumGenerator;

import java.util.Random;
import java.util.Scanner;

public class guessNumberRun {
    public static void main(String[] args) {
        Random random = new Random();
        RandomNumGenerator randomNumGenerator = new RandomNumGenerator(random);
        Scanner scanner = new Scanner(System.in);
        GuessNumberGame guessNumberGame = new GuessNumberGame(scanner, randomNumGenerator);
        guessNumberGame.run();
    }
}
