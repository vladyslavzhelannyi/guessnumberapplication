package test.java.com.guessNumber.utils;

import main.java.com.guessNumber.utils.RandomNumGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Random;

public class RandomNumGeneratorTest {
    Random random = Mockito.mock(Random.class);
    RandomNumGenerator cut = new RandomNumGenerator(random);

    static Arguments[] getRandomNumInRangeTestArgs(){
        return new Arguments[]{
          Arguments.arguments(4, 32, 16, 20),
          Arguments.arguments(74, 91, 3, 77)
        };
    }

    @ParameterizedTest
    @MethodSource("getRandomNumInRangeTestArgs")
    void getRandomNumInRangeTest(int minNum, int maxNum, int randomNumber, int expected){
        Mockito.when(random.nextInt(maxNum - minNum + 1)).thenReturn(randomNumber);
        int actual = cut.getRandomNumInRange(minNum, maxNum);
        Assertions.assertEquals(expected, actual);
    }
}
