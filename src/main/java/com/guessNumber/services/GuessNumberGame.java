package main.java.com.guessNumber.services;

import main.java.com.guessNumber.utils.RandomNumGenerator;

import java.util.Scanner;

public class GuessNumberGame {
    private static final String MSG_STANDARD_RULE = "Вам предлагается угадать число от 1 до 100 за 5 попыток";
    private static final String MSG_CHANGE_RANGE_QUESTION = "Хотите ли вы изменить диапазон чисел?\nНапишите \"да\" " +
            "в случае, если хотите изменить диапазон или же любую клавишу в случае, если не желаете менять диапазон, " +
            "после чего нажмите Enter";
    private static final String MSG_CHANGE_NUM_ATTEMPTS_QUESTION = "Хотите ли вы изменить количество попыток угадать " +
            "число?\nНапишите \"да\" в случае, если хотите изменить количество попыток или же любую клавишу в случае, " +
            "если не желаете менять диапазон, после чего нажмите Enter";
    private static final String RESPONSE_YES = "да";
    private static final String EXIT = "exit";

    private Scanner scanner;
    private RandomNumGenerator randomNumGenerator;
    private int numberToGuess;
    private int minRangeVal = 1;
    private int maxRangeVal = 100;
    private int attemptsNum = 5;
    private int attemptsUsed = 0;
    private int attemptsLeft = attemptsNum - attemptsUsed;
    private int previousAttempt;
    private int userAttempt;
    private int enteredVal;
    private String changeRangeDecision;
    private String changeAttemptsNumDecision;

    public GuessNumberGame(Scanner scanner, RandomNumGenerator randomNumGenerator) {
        this.scanner = scanner;
        this.randomNumGenerator = randomNumGenerator;
    }

    public void run(){
        setRules();
        playGame();
    }

    private void setRules(){
        System.out.println(MSG_STANDARD_RULE + "\n" + MSG_CHANGE_RANGE_QUESTION);
        changeRangeDecision = scanner.nextLine();
        if (changeRangeDecision.equalsIgnoreCase(RESPONSE_YES)) {
            changeRange();
        }
        else if(changeRangeDecision.equalsIgnoreCase(EXIT)){
            exit();
        }

        System.out.println(MSG_CHANGE_NUM_ATTEMPTS_QUESTION);
        changeAttemptsNumDecision = scanner.nextLine();
        if (changeAttemptsNumDecision.equalsIgnoreCase(RESPONSE_YES)) {
            changeAttemptsNum();
        }
        else if (changeAttemptsNumDecision.equalsIgnoreCase(EXIT)){
            exit();
        }
    }

    private void changeRange(){
        System.out.println("Введите минимальное значение диапазона в пределах от 1 до 200 включительно");
        while (true) {
            if(scanner.hasNextInt()){
                enteredVal = scanner.nextInt();
                if(enteredVal < 1 || enteredVal > 200){
                    System.out.println("Минимальное значение может быть выбрано в диапазоне от 1 до 200. Повторите ввод");
                }
                else{
                    minRangeVal = enteredVal;
                    clearScannerBuffer();
                    break;
                }
            }
            else if (scanner.hasNextDouble()) {
                System.out.println("Значения с плавающей точкой недопустимы. Повторите ввод");
                clearScannerBuffer();
            }
            else if (scanner.next().equalsIgnoreCase(EXIT)){
                exit();
            }
            else{
                System.out.println("Вы ввели недопустимые символы. Повторите ввод");
                clearScannerBuffer();
            }
        }

        System.out.println("Введите максимальное значение диапазона в пределах от 1 до 200 включительно");
        while (true) {
            if (scanner.hasNextInt()) {
                enteredVal = scanner.nextInt();
                if (enteredVal < 1 || enteredVal > 200) {
                    System.out.println("Максимальное значение может быть выбрано в диапазоне от 1 до 200. Повторите ввод");
                }
                else if (enteredVal < minRangeVal) {
                    System.out.println("Максимальное значение не может быть меньше, чем минимальное. Повторите ввод");
                } else {
                    maxRangeVal = enteredVal;
                    clearScannerBuffer();
                    break;
                }
            } else if (scanner.hasNextDouble()) {
                System.out.println("Значения с плавающей точкой недопустимы. Повторите ввод");
                clearScannerBuffer();
            } else if (scanner.next().equalsIgnoreCase("exit")) {
                exit();
            } else {
                System.out.println("Вы ввели недопустимые символы. Повторите ввод");
                clearScannerBuffer();
            }
        }

    }

    private void changeAttemptsNum(){
        System.out.println("Введите количество попыток в пределах от 1 до 15 включительно");
        while (true) {
            if(scanner.hasNextInt()){
                enteredVal = scanner.nextInt();
                if(enteredVal < 1 || enteredVal > 15){
                    System.out.println("Количество попыток должно быть в пределах от 1 до 15. Повторите ввод");
                }
                else{
                    attemptsNum = enteredVal;
                    clearScannerBuffer();
                    break;
                }
            }
            else if (scanner.hasNextDouble()) {
                System.out.println("Значения с плавающей точкой недопустимы. Повторите ввод");
                clearScannerBuffer();
            }
            else if (scanner.nextLine().equalsIgnoreCase(EXIT)){
                exit();
            }
            else{
                System.out.println("Вы ввели недопустимые символы. Повторите ввод");
                clearScannerBuffer();
            }
        }
    }

    private void playGame(){
        createNumber();
        guessNumber();
    }

    private void createNumber(){
        numberToGuess = randomNumGenerator.getRandomNumInRange(minRangeVal, maxRangeVal);
        System.out.println("Привет, я загадал число от min до max вашего диапазона. Попробуй угадать его за x попыток!");
    }

    private void guessNumber(){
        while(true) {
            if (attemptsUsed == attemptsNum) {
                System.out.println("К сожалению попытки угадать закончились");
                break;
            }
            if(scanner.hasNextInt()) {
                attemptsUsed++;
                userAttempt = scanner.nextInt();
                if (userAttempt == numberToGuess) {
                    System.out.println("Поздравляю! Ты угадал задуманное число за " + attemptsUsed + " попыток");
                    exit();
                }
                else {
                    if (attemptsUsed > 1) {
                        if (Math.abs(userAttempt - numberToGuess) > Math.abs(previousAttempt - numberToGuess)) {
                            System.out.println("Не угадал, холоднее!!! Осталось " + attemptsLeft + " попыток ");
                        }
                        else if (Math.abs(userAttempt - numberToGuess) < Math.abs(previousAttempt - numberToGuess)) {
                            System.out.println("Не угадал, но теплее!!! Осталось " + attemptsLeft + " попыток ");
                        }
                        else if (userAttempt == previousAttempt){
                            System.out.println("Вы вывели то же число, что и в прошлый раз");
                        }
                    }
                    else{
                        System.out.println("Не угадал. Попробуй снова");
                    }
                    previousAttempt = userAttempt;
                }
            }
            else if (scanner.hasNextDouble()) {
                System.out.println("Значения с плавающей точкой недопустимы. Повторите ввод");
                clearScannerBuffer();
            }
            else {
                System.out.println("Ввод некорректный. Необходимо ввести целочисленное значение в пределах выбранного диапазона");
                clearScannerBuffer();
            }
        }
    }

    private void exit(){
        System.exit(0);
    }

    private void clearScannerBuffer(){
        scanner.nextLine();
    }
}
