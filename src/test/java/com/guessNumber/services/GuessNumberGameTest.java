package test.java.com.guessNumber.services;

import main.java.com.guessNumber.services.GuessNumberGame;
import main.java.com.guessNumber.utils.RandomNumGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;
import java.util.Scanner;

public class GuessNumberGameTest {
    Scanner scanner = Mockito.mock(Scanner.class);
    RandomNumGenerator randomNumGenerator = Mockito.mock(RandomNumGenerator.class);

    GuessNumberGame cut = new GuessNumberGame(scanner, randomNumGenerator);

    static Arguments[] runTestArgs(){
        return new Arguments[]{
          Arguments.arguments(14, 23, 3, 20, 22, 17, 15, "да", true),
          Arguments.arguments(102, 142, 3, 131, 127, 105, 105, "да", true),
        };
    }

    @ParameterizedTest
    @MethodSource("runTestArgs")
    void runTest(int min, int max, int attempts, int firstAttempt, int secondAttempt, int thirdAttempt,
                 int hiddenNumber, String answer, boolean isInt){
        Mockito.when(scanner.nextLine()).thenReturn(answer).thenReturn(answer);
        Mockito.when(scanner.nextInt()).thenReturn(min).thenReturn(max).thenReturn(attempts).thenReturn(firstAttempt)
                .thenReturn(secondAttempt).thenReturn(thirdAttempt);
        Mockito.when(scanner.hasNextInt()).thenReturn(isInt);
        Mockito.when(randomNumGenerator.getRandomNumInRange(min, max)).thenReturn(hiddenNumber);


        cut.run();
    }
}
